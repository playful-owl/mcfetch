#' contains remote paths
#' @export
paths <- list(siteids="/api/get/siteids",
              series_by_siteid="/api/get/series_by_siteid/",
              data_by_series_id="/api/get/data_by_series_id/")
